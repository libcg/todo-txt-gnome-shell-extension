#!/bin/sh
if [ -z "$1" ]; then
    echo "Usage $0 <version>"
    exit 1
fi
version=$1

set -e
./json2schema.py
if [ -z "${ZIP_NAME}" ]; then
    zip_file="todo.txt@bart.libert.gmail.com-v${version}.zip"
else
    zip_file=${ZIP_NAME}
fi
rm -f "${zip_file}"
case ${version} in
    ''|*[!0-9]*) jq '.version="'${version}'"' metadata.json | sponge metadata.json ;;
    *) jq '.version='${version} metadata.json | sponge metadata.json ;;
esac
if [ -z "${NO_COMMIT}" ]; then
    git add metadata.json
    git commit -m "Update metadata"
    git tag -a -s "v${version}"
fi
glib-compile-schemas schemas
zip "${zip_file}" -MM -T -@ < dist_files.lst
